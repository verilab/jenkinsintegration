#!/bin/sh -xe
#************************************************************************
# Copyright (c) 2013 Verilab
# Author: Andre Winkelmann
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#************************************************************************

if [ "$MY_REGRESSION_AREA" = "" ]; then
  MY_REGRESSION_AREA=regression;
  export MY_REGRESSION_AREA;
fi    

if [ "$MY_VMANAGER_CS_PROFILE" = "" ]; then
    /bin/echo $0: Error: MY_VMANAGER_CS_PROFILE is not defined.
    /bin/echo Please set it to the vManger CS profile path to be
    /bin/echo used for this session.
    exit 1
fi

if [ "$MY_VMANAGER_CSV_VIEW" = "" ]; then
    /bin/echo $0: Warning: MY_VMANAGER_CSV_VIEW is not defined.
    /bin/echo Please set it to the vManger CS view name to be used for CSV exporting
    /bin/echo Using default value of "vlabCSV(${USER})"
    export MY_VMANAGER_CSV_VIEW="vlabCSV(${USER})"
fi

if vm_root 1>/dev/null 2>&1;then
  :;
else 
  echo "*** Error: PATH to vManager is not set. Please set your UNIX path";
  echo "           to <vManager-install-dir>/bin and restart this demo";
  exit 1;
fi;

export VLAB_TESTS=`pwd`/../sv/tests
export VLAB_VSIF=`pwd`/../sv/sv_tests.vsif

vmanager -cs -profile ${MY_VMANAGER_CS_PROFILE} -exec scripts/run.tcl

python ../scripts/csv_to_junit_reporter.py regression_tests.csv junit.xml
