'''
Copyright 2014 Gordon McGregor, Andre Winkelmann

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

from junit_jenkins_reporter import JunitReport
import csv
import sys
import re
import os

csv.register_dialect('escaped', escapechar='\\', doublequote=False, quoting=csv.QUOTE_NONE)

# CSV formating rules, escaping characters etc http://pymotw.com/2/csv/


def csv_to_junit(csv_filename, outfile):
    '''Given a CSV format input file, generate a Jenkins compatible JUnit result'''
    testsuites={}
    with open(csv_filename) as csv_file:
        csv_data = csv.reader(csv_file, dialect='excel')
        report = JunitReport()


        for (rownum, line) in enumerate(csv_data): 
            print rownum, line
            if rownum == 0:
                # create a lookup table (lu) of column titles (from the first row) to array index values,
                # so we can access row data via names and allow columns to move
                lu = dict((k, i) for (i, k) in enumerate(line))

            if rownum and line: #any row other than the first is data, with valid data
                # replace slashes with dots, and remove any leading or trailing dots
                test_group = re.sub(r'\/', ".", line[lu['Test Group']])
                test_group = re.sub(r'^\.', "", test_group)
                test_group = re.sub(r'\.$', "", test_group)
                test_group += "." + line[lu['Test Name']]

                if test_group not in testsuites:
                    # its a new testsuite to create a dictionary with a reference to the testsuite
                    # XML element and track number of passes and fails to update at the end
                    testsuites[test_group] = {}
                    testsuites[test_group]['element'] = report.add_testsuite(
                                                            name=test_group,
                                                            id = str(len(testsuites)-1),
                                                            time="",
                                                            failures="",
                                                            passes="")
                    testsuites[test_group]['failures'] = 0
                    testsuites[test_group]['passes'] = 0

                report.add_testcase(
                    name=str(rownum),
                    testsuite=testsuites[test_group]['element'],
                    classname=test_group,
                    time="%.2f" % (float(line[lu['CPU Time (ms.)']])/1000))

                if line[lu['Status']] == 'failed':
                    testsuites[test_group]['failures'] += 1
                    fail_desc  = 'Seed: ' + line[lu['Seed']] + '\n'
                    fail_desc += 'SV Seed: ' + line[lu['SV Seed']] + '\n'
                    
                    log_files = set(line[lu['Log File']].split('<__SEPARATOR__>'))
                    if "JOB_URL" in os.environ and "WORKSPACE" in os.environ:
                        log_files = map(lambda f: f.replace(os.environ['WORKSPACE'] + '/', os.environ['JOB_URL'] + 'ws/'), log_files)
                    
                    fail_desc += 'Log Files: ' + "\n".join(log_files) + '\n'
                    fail_desc += 'Complete Message: \n' + line[lu['First Failure Description']]
                    report.add_failure(
                        type="Fail",
                        msg_text=fail_desc,
                        message=line[lu['First Failure Description']].split('\n')[0])
                else:
                    testsuites[test_group]['passes'] += 1

        # Update pass/ fail counts for all the testsuites that have been recorded
        for suite in testsuites:
            report.update_testsuite(
                testsuite=testsuites[suite]['element'],
                failures=testsuites[suite]['failures'],
                passes=testsuites[suite]['passes'])
        report.write(outfile)

 
if __name__ == '__main__':
    csv_to_junit(sys.argv[1], sys.argv[2])
