'''
Copyright 2013 Gordon McGregor

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

import xml.etree.ElementTree as ET
import mmap
import sys

node = ET.Element
subnode = ET.SubElement

# Some JUnit format info http://nelsonwells.net/2012/09/how-jenkins-ci-parses-and-displays-junit-output/

# more help http://windyroad.com.au/dl/Open%20Source/JUnit.xsd
# Parsed by Jenkins in the source code files in https://github.com/jenkinsci/jenkins/tree/master/core/src/main/java/hudson/tasks/junit  particularly TestResult.java, ClassResult.java and CaseResult.java

TRUNCATE_LINES = 50

class File(file):
# File class from  http://stackoverflow.com/questions/136168/get-last-n-lines-of-a-file-with-python-similar-to-tail

    def countlines(self):
        buf = mmap.mmap(self.fileno(), 0)
        lines = 0
        while buf.readline():
            lines += 1
        return lines


    def head(self, lines_2find=1):
        self.seek(0)                            #Rewind file
        return [self.next() for x in xrange(lines_2find)]


    def tail(self, lines_2find=1):
        self.seek(0, 2)                         #go to end of file
        bytes_in_file = self.tell()
        lines_found, total_bytes_scanned = 0, 0
        while (lines_2find+1 > lines_found and
               bytes_in_file > total_bytes_scanned):
            byte_block = min(1024, bytes_in_file-total_bytes_scanned)
            self.seek(-(byte_block+total_bytes_scanned), 2)
            total_bytes_scanned += byte_block
            lines_found += self.read(1024).count('\n')
        self.seek(-total_bytes_scanned, 2)
        line_list = list(self.readlines())
        return line_list[-lines_2find:]



class JunitReport:
    '''Build a JUnit format report file for Jenkins and/or Hudson.
       most of the kwargs allow you to pass name=value pairs straight into the tag, to become attributes in the final output'''

    results = None
    last_testsuite = None
    last_testcase = None

    def __init__(self):
        '''Class constructor, creates top level testsuites container for results'''
        self.results = node("testsuites", name="results")


    def add_testsuite(self, **kwargs):
        '''Add a new testsuite and associated attributes [name, time]'''
        self.last_testsuite = subnode(self.results, "testsuite", **kwargs)
        return self.last_testsuite


    def add_testcase(self, testsuite=None, **kwargs):
        '''Add a new testcase and associated attributes [name, time]'''
        if testsuite == None:
            testsuite = self.last_testsuite
        self.last_testcase = subnode(testsuite, "testcase", **kwargs)
        return self.last_testcase


    def update_testsuite(self, testsuite=None, **kwargs):
        '''Add additional arguments to the testsuite tag (usually pass/ fail results as a a result of processing all of the tests in the list)'''
        if testsuite == None:
            testsuite = self.last_testsuite
        for k in kwargs:
            testsuite.set(k, str(kwargs[k]))


    def update_testsuites(self, **kwargs):
        '''Add additional arguments to the testsuites tag (usually pass/ fail results as a a result of processing all of the tests in the list)'''
        for k in kwargs:
            self.results.set(k, str(kwargs[k]))


    def add_log(self, logfile, testcase=None):
        '''Given a logfile, head/tail truncate it to TRUNCATE_LINES*2 length and store in a <system-out> tag'''
        if testcase == None:
            testcase = self.last_testcase
        log = subnode(testcase, "system-out")
        f = File(logfile, 'r+')
        lines = f.countlines()
        if lines > (TRUNCATE_LINES * 2):
            head = f.head(TRUNCATE_LINES)
            tail = f.tail(TRUNCATE_LINES)
            log.text = "".join(head + list("[...truncated %d lines...]\n" % ( (lines - (TRUNCATE_LINES*2)) )) + tail)
        else:
            log.text = "".join(f.readlines())


    def add_failure(self, msg_text="", testcase=None, **kwargs):
        '''Update the testcase, or last_testcase if none provided to be a failure and add appropriate attributes [ type and message ]'''
        if testcase == None:
            testcase = self.last_testcase
        log = subnode(testcase, "failure", **kwargs)
        log.text = msg_text


    def indent(self, elem, level=0):
        '''Pretty printer for the XML output'''
        i = "\n" + level*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def write(self, filename):
        self.indent(self.results)
        #ET.ElementTree(self.results).write(filename, xml_declaration = True, method = "xml", encoding="UTF-8") # later API
        ET.ElementTree(self.results).write(filename, "UTF-8")



