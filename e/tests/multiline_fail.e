//************************************************************************
// Copyright (c) 2013 Verilab
// Author: Andre Winkelmann
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//************************************************************************

<'

extend sys {
  setup() is also {
    // Let Specman not stop on the first error
    set_check("...", ERROR_CONTINUE);
  };
  
  run() is also {
    out("Running a failing test with more than one error but only the first should be shown in Jenkins!");
    dut_error("Please Jenkins, show this error message!") {
      out("Let us see more details of the error.");
      out("What could have gone wrong?");
      out("... no idea ...");
    };
    message(NONE, "This test is doing a lot of stuff, even more than the one before.");
    error("If Jenkins shows this message, something went wrong somewhere!");
  };
};

'>
