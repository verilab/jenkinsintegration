//************************************************************************
// Copyright (c) 2013 Verilab
// Author: Andre Winkelmann
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//************************************************************************
//
// Usage
//   No matter through which command or ecom file you need to execute the
//   following two statements:
//
//     load path/to/vlab_post_session.e;
//     sys.vlab_post_session_analysis();
//
//   If you are using vm_launch.pl/emanager/vmanager to start your
//   regression add the following to your arguments:
//   
//     -commands "load path/to/vlab_post_session.e; sys.vlab_post_session_analysis();"
//
//************************************************************************


<'
// Somehow having all this code just in an ecom file does not work
extend sys {

  vlab_vsof: vm_vsof;
  vlab_runs: list of vm_run;
  
  
  // Write out text file which contains either PASS or FAIL depending on overall session result
  vlab_output_overall_pass_or_fail_result() is {
    var session_failures: list of vm_failure = vlab_vsof.get_failures();
    var f:file = files.open("regression_result.txt", "w", "Overall session pass fail text file");
    var res:bool = TRUE;
    
    if !session_failures.is_empty() {
      res = FALSE;
    };
    
    for each in vlab_runs {
      if !it.has_passed() {
        res = FALSE;
        break;
      };
    };
    
    if res == TRUE {
      files.write(f, "PASS");
    } else {
      files.write(f, "FAIL");
    };
    
    files.close(f);
    out("Written regression_result.txt");
  };


  // Write out CSV file containing individual run results
  vlab_output_per_test_results_as_csv() is {
    var f:file = files.open("regression_tests.csv", "w", "Test list csv file");
    files.write(f, "Test Group,Test Name,Seed,SV Seed,Status,CPU Time (ms.),Log File,First Failure Description");

    for each in vlab_runs {
      var first_fail : string = it.get_attribute_value(vm_manager.get_attribute_by_name("first_failure_description"));
      first_fail = str_replace(first_fail, "/\"/", "\"\"");

      files.write(f, append(
        it.get_attribute_value(vm_manager.get_attribute_by_name("test_group")), ",",
        it.get_attribute_value(vm_manager.get_attribute_by_name("test_name")), ",",
        it.get_attribute_value(vm_manager.get_attribute_by_name("seed")), ",",
        it.get_attribute_value(vm_manager.get_attribute_by_name("sv_seed")), ",",
        it.get_attribute_value(vm_manager.get_attribute_by_name("status")), ",",
        appendf("%.2f", it.get_attribute_value(vm_manager.get_attribute_by_name("cpu_time")).as_a(real)), ",",
        it.get_attribute_value(vm_manager.get_attribute_by_name("log_file")), ",",
        "\"", first_fail, "\""
      ));
    };
 
    files.close(f);
    out("Written regression_tests.csv");
  };

  vlab_post_session_analysis() is {
  
    out("Waiting until all tests have been executed");
    vm_util.trace_all_sessions_to_finish();
    out("All tests should be done by now. Starting post session analysis.");

    vlab_vsof = vm_manager.get_all_sessions()[0];  // or load a specific vsof
    vlab_runs = vlab_vsof.get_runs();
    
    vlab_output_overall_pass_or_fail_result();
    vlab_output_per_test_results_as_csv();
  };
};

'>
