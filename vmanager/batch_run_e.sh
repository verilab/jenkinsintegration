#!/bin/sh -xe
#************************************************************************
# Copyright (c) 2013 Verilab
# Author: Andre Winkelmann
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#************************************************************************

if [ "$MY_REGRESSION_AREA" = "" ]; then
  MY_REGRESSION_AREA=regression;
  export MY_REGRESSION_AREA;
fi    

if vm_root 1>/dev/null 2>&1;then
  :;
else 
  echo "*** Error: PATH to vManager is not set. Please set your UNIX path";
  echo "           to <vManager-install-dir>/bin and restart this demo";
  exit 1;
fi;

if sn_root 1>/dev/null 2>&1;then
  :;
else 
  echo "*** Error: PATH to Specman is not set. Please set your UNIX path";
  echo "           to <Specman-install-dir>/bin and restart this demo";
  exit 1;
fi;


SPECMAN_PATH=${SPECMAN_PATH}:`pwd`/..
export SPECMAN_PATH;
echo $SPECMAN_PATH

vm_launch.pl \
  -vsif ../e/e_tests.vsif \
  -batch \
  -command "load scripts/vlab_post_session.e; sys.vlab_post_session_analysis();" \
  -output_mode log_only \
  -regr_report

python ../scripts/csv_to_junit_reporter.py regression_tests.csv junit.xml

# Create links to latest HTML report if it exists
sleep 10
SESSION_DIR=`ls -1rt $MY_REGRESSION_AREA | tail -n 1`
if [ -e "$MY_REGRESSION_AREA/$SESSION_DIR/Regression_Report.html" ]; then
  rm -rf html_report
  mkdir html_report
  cp $MY_REGRESSION_AREA/$SESSION_DIR/Regression_Report.html       html_report/index.html
  ln -s $MY_REGRESSION_AREA/$SESSION_DIR/Regression_Report.html.files html_report/Regression_Report.html.files
fi

grep PASS regression_result.txt
